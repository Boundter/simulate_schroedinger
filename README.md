This is a script to calculate the Eigenvalues and Eigenfunctions of the Schrödinger-Equation.

The file code/main.py is outdated, code/main2.py should be used. It changes the style of programming from fruntional to object-oriented. The plot should be called from the main-directory.
